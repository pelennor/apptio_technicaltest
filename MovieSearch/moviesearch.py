#!/usr/bin/python

import argparse
import requests
import urllib

def main():

    apikey = 'f194ef40'

    baseURL = "http://www.omdbapi.com/?apikey=%s&" % apikey
 

    parser = argparse.ArgumentParser()

    parser.add_argument("searchtype" , help="type of search", choices = ("id", "onetitle", "alltitles") )
    parser.add_argument("searchquery" , help="Title or ID to search on" )

    pargs = parser.parse_args()
    searchQuery = urllib.parse.quote_plus(pargs.searchquery)

    if pargs.searchtype == 'id' :
        finalURL = '%si=%s' % (baseURL, searchQuery)
    elif pargs.searchtype == "onetitle" :
        finalURL = '%st=%s' % (baseURL, searchQuery)
    elif pargs.searchtype == "alltitles" :
        finalURL = '%ss=%s' % (baseURL, searchQuery)


    print(finalURL)
    response = requests.get(finalURL)
    print(response.text)


if __name__ == "__main__" :
    main()

